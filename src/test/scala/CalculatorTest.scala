import org.scalatest.funspec.AnyFunSpec
import CalculatorApp._

class CalculatorTest extends AnyFunSpec {

  val calculator = new Calculator

  describe("multiplication") {
    it("should return 0 by multiply to 0") {
      assert(calculator.multiply(10, 0) == 0)
      assert(calculator.multiply(-10, 0) == 0)
    }
    it("should return 20 by multiply 10 to 2") {
      assert(calculator.multiply(10, 2) == 20)
    }
  }

}
