ThisBuild / version := "0.1.1"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(
    name := "myscalaapp-cicd-project"
  )

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.7",
  "org.scalatest" %% "scalatest" % "3.2.7" % "test",
)